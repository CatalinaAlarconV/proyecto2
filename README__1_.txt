
Este programa se ha creado exclusivamente para ser utilizado en servicios públicos o privados de salud, en donde se podrán registrar los datos del paciente ya sea su nombre, el rut, la hora en la que se atenderá y el médico que lo observará. Los principales usuarios que manipularan el programa son doctores y secretarias del servicio. 

INDICACIONES PARA EL BUEN USO DEL PROGRAMA

-Puede ser utilizado cuantas veces uno quiera. 
-Solo tiene autorización ingresar pacientes la secretaria. 
-solo tiene autorización el doctor de ingresar estado de salud del paciente. 
-Al guardar los datos, se registran en el programa y al momento de cerrarlo y abrirlo nuevamente no se borran.
-Se puede acceder al registro de médicos y con que pacientes se atienden en el símbolo de búsqueda.
-se puede acceder a pacientes ingresados solo su rut. 
-solo tiene autorización la secretaria para ver número de pacientes y para acceder a la gráfica de ellos. 


Reglas PEP8 utilizadas en el proyecto:

Estas son algunas de las reglas más básicas de PEP8 (Python Enhancement Proposals 8, Propuestas para mejorar Python)que se emplearon y estas más que reglas, son acuerdos y consejos a seguir para lograr un código ordenado y legible, que permita ser entendido por cualquier programador.

-La identación está hecha  con 4 espacios por nivel.

-Nunca mezclamos espacios con tabuladores.

-Separamos funciones de alto nivel y definiciones de clase con dos líneas en blanco.
 
-Los métodos definidos dentro de una clase están separados por una línea en blanco.

-Las importaciones por las aplicamos por lineas separadas. 

-Evitamos dejar espacios en blanco donde no correspondía. 

Brindamos este programa al servicio público de salud para que haya un orden en el registro de los pacientes y una mejora del sistema de atención.
